"""
Replicates experiment 1 of 'Learning to Protect Communications with Adversarial
Neural Cryptography' by Abadi and Andersen.

Trains 20 different Alice-Bob-Eve configurations using identical parameters to
those presented in Abadi and Andersen, saving major results for use in figure
generation.
"""


from keras import backend as K
from keras.layers import Input, concatenate, Conv1D, Dense, Reshape
from keras.losses import mae
from keras.models import Model, Sequential
from keras.optimizers import Adam
from numpy.random import randint
import json
import matplotlib.pyplot as plt
import numpy as np
import os


def main():
    experiment_1()

    experiment_1_report()


def experiment_1(replicates=20,
                 comm_epochs=150000,
                 crack_tries=5,
                 crack_epochs=250000):
    """
    Args:
        replicates: int (default: 20)
            number of experiment replicates to perform.

        comm_epochs: int (default: 150000)
            number of epochs to train the initial configuration.

        crack_tries: int (default: 5)
            number of Eves to retrain.

        crack_epochs: int (default: 250000)
            number of epochs to retrain each Eve.
    """
    os.makedirs('experiment_1', exist_ok=True)

    for i in range(replicates):
        alice, bob_errors, eve_errors = experiment_1_train(epochs=comm_epochs)
        alice.save('experiment_1/alice_{}.h5'.format(i))
        bob.save('experiment_1/bob_{}.h5'.format(i))
        save_errors(bob_errors, i, path='experiment_1/bob_errors')
        save_errors(eve_errors, i, path='experiment_1/eve_errors')

        for j in range(crack_tries):
            crack_error = retrain_eve(alice, epochs=crack_epochs)
            save_errors(crack_error,
                        '({}, {})'.format(i, j),
                        path='experiment_1/crack_errors')


def experiment_1_report(replicates=20,
                        crack_tries=5,
                        N=16,
                        bob_thresh=0.05,
                        eve_thresh=2.):
    """
    Args:
        replicates: int (default: 20)
            number of experiment replicates performed.

        crack_tries: int (default: 5)
            number of retrained Eves.

        N: int (default: 16)
            Length of messages (and keys) used for training.

        bob_thresh: float (default: 0.05)
            Error threshold which determines if Alice and Bob have been
            successfully trained.

        eve_thresh: float (default: 2.)
            Error threshold which determines if the best retrained Eve
            succeeded at decoding Alice and Bob's communication.
    """
    with open('experiment_1/bob_errors.json', 'r') as f:
        bob_errors = json.load(f)

    with open('experiment_1/eve_errors.json', 'r') as f:
        eve_errors = json.load(f)

    with open('experiment_1/crack_errors.json', 'r') as f:
        crack_errors = json.load(f)

    pass_count = 0
    for i in range(replicates):
        bob_error = bob_errors[str(i)][-1]
        eve_error = eve_errors[str(i)][-1]
        crack_error = []
        for key, value in crack_errors.items():
            if '({},'.format(i) in key:
                crack_error.append(min(value))
        crack_error = min(crack_error)

        passed = True
        if bob_error >= bob_thresh:
            passed = False

        if (N / 2 - eve_error) >= eve_thresh:
            passed = False

        if (N / 2 - crack_error) >= eve_thresh:
            passed = False

        if passed:
            pass_count += 1

        print('Model {} Results'.format(i))
        print('\tBob error:   {:0.4}    ({})'.format(bob_error, bob_thresh))
        print('\tEve error:   {:0.4}    ({})'.format(eve_error, eve_thresh))
        print('\tCrack error: {:0.4}    ({})'.format(crack_error, eve_thresh))
        print('\tPassed:      {}\n'.format(passed))

    print('Pass rate: {}/{} or {:0.4}%'.format(pass_count,
                                               replicates,
                                               pass_count / replicates))


def gen_figure_2(N=16, fig_name='figure_2', file_type='pdf'):
    """
    Args:
        N: int (default: 16)
            Length of messages (and keys) used for training.

        fig_name: str (default: 'training_error')
            Name of the figure when saved.

        file_type: str (defualt: 'pdf')
            Desired file extension/format of the saved figure.
    """
    alice, bob_errors, eve_errors = experiment_1_train(epochs=25000,
                                                       N=N)

    fig, ax = plt.subplots()
    ax.plot(np.arange(bob_errors.shape[0]),
            bob_errors,
            'r-',
            linewidth=1,
            label='Bob')
    ax.plot(np.arange(eve_errors.shape[0]),
            eve_errors,
            'g-',
            linewidth=1,
            label='Eve')

    ax.set_xlabel('Steps')
    ax.set_ylabel('Bits wrong (of {})'.format(N))

    ax.legend()

    plt.grid(True)

    plt.savefig('{}.{}'.format(fig_name, file_type), dpi=96)


def experiment_1_train(alice=None,
                       bob=None,
                       eve=None,
                       epochs=10,
                       N=16,
                       comm_iter=1,
                       crack_iter=2,
                       batch_size=4096):
    """
    Args:
        alice: keras.models.Sequential (default: None)
            Allows for a pre-trained Alice network to be reused.

        bob: keras.models.Sequential (default: None)
            Allows for a pre-trained Bob network to be reused.

        eve: keras.models.Sequential (default: None)
            Allows for a pre-trained Eve network to be reused.

        epochs: int (default: 10)
            Number of training epochs.

        N: int (default: 16)
            Length of messages (and keys) used for training.

        comm_iter: int (default: 1)
            Number of Alice-Bob updates per epoch.

        crack_iter: int (default: 2)
            Number of Eve updates per epoch.

        batch_size: int (default: 4096)
            Number of training samples per batch.

    Returns: tuple
        (alice, bob_error, eve_error)

        A tuple containing the trained Alice, Bob's training errors and Eve's
        training errors.
    """
    # Build disjoint network components
    if not alice:
        alice = replicate_alice(N)

    if not bob:
        bob = replicate_alice(N)

    if not eve:
        eve = replicate_eve(N)

    # Symbolic inputs for plain text and private key
    message = Input(shape=(N,))
    key = Input(shape=(N,))

    # Alice network I/O
    alice_in = concatenate([message, key])
    alice_pred = alice(alice_in)

    # Bob network I/O
    bob_in = concatenate([alice_pred, key])
    bob_pred = bob(bob_in)

    # Eve network I/O
    eve_pred = eve(alice_pred)

    comm_model = Model(inputs=[message, key], outputs=[bob_pred, eve_pred])

    crack_model = Model(inputs=[message, key], outputs=eve_pred)

    # Compile communication network so that Eve is frozen
    eve.trainable = False

    comm_model.compile(optimizer=Adam(lr=0.0008),
                       loss=[sae, crack_loss],
                       loss_weights=[1., 1.])

    # Compile crack network so that Alice is frozen
    eve.trainable = True
    alice.trainable = False

    crack_model.compile(optimizer=Adam(lr=0.0008), loss=sae)

    # Training
    bob_errors = np.zeros(epochs)
    eve_errors = np.zeros(epochs)

    for i in range(epochs):
        for j in range(comm_iter):
            messages, keys = generate_batch(N)
            comm_model.train_on_batch([messages, keys], [messages, messages])

        for k in range(crack_iter):
            messages, keys = generate_batch(N)
            crack_model.train_on_batch([messages, keys], messages)

        messages, keys = generate_batch(N)
        _, bob_errors[i], _ = comm_model.test_on_batch([messages, keys],
                                                       [messages, messages])
        eve_errors[i] = crack_model.test_on_batch([messages, keys], messages)

    return alice, bob_errors, eve_errors


def retrain_eve(alice,
                epochs=250000,
                N=16,
                crack_iter=1,
                batch_size=4096):
    """
    Args:
        alice: keras.models.Sequential
            A pre-trained Alice network which will be tested.

        epochs: int (default: 250000)
            Number of training epochs.

        N: int (default: 16)
            Length of messages (and keys) used for training.

        crack_iter: int (default: 1)
            Number of Eve updates per epoch.

        batch_size: int (default: 4096)
            Number of training samples per batch.

    Returns: np.ndarray
        Training errors of Eve network.
    """
    # Build disjoint network components
    eve = replicate_eve(N)

    # Symbolic inputs for plain text and private key
    message = Input(shape=(N,))
    key = Input(shape=(N,))

    # Alice network I/O
    alice_in = concatenate([message, key])
    alice_pred = alice(alice_in)

    # Eve network I/O
    eve_pred = eve(alice_pred)

    crack_model = Model(inputs=[message, key], outputs=eve_pred)

    # Compile crack network so that Alice is frozen
    eve.trainable = True
    alice.trainable = False

    crack_model.compile(optimizer=Adam(lr=0.0008), loss=sae)

    # Training
    eve_errors = np.zeros(epochs)

    for i in range(epochs):
        for k in range(crack_iter):
            messages, keys = generate_batch(N)
            crack_model.train_on_batch([messages, keys], messages)

        messages, keys = generate_batch(N)
        eve_errors[i] = crack_model.test_on_batch([messages, keys], messages)

    return eve_errors


def save_errors(errors, i, file_name=None):
    """
    Args:
        errors: np.ndarray
            A series of errors to serialize.

        i: int
            An integer label for the errors.

        file_name: str (default: None)
            Desired file name excluding file extension.
    """
    errors = {'{}'.format(i): list(errors)}

    if not file_name:
        file_name = 'errors'

    try:
        with open('{}.json'.format(file_name), 'r') as f:
            old_errors = json.load(f)

        errors = {**old_errors, **errors}

    except FileNotFoundError:
        pass

    with open('{}.json'.format(file_name), 'w') as f:
        json.dump(errors, f)


def replicate_alice(N):
    """Constructs and randomly initializes an Alice network.

    Also constructs Bob networks, since they share the same architecture.

    Args:
        N: int
            Length of messages (and keys) used for training.

    Returns: keras.models.Sequential
    """
    return Sequential([
        Dense(2 * N, activation='sigmoid', input_shape=(2 * N, )),
        Reshape((2 * N, 1)),
        Conv1D(2, 4, strides=1, padding='same', activation='sigmoid'),
        Conv1D(4, 2, strides=2, padding='same', activation='sigmoid'),
        Conv1D(4, 1, strides=1, padding='same', activation='sigmoid'),
        Conv1D(1, 1, strides=1, padding='same', activation='tanh'),
        Reshape((N, ))
    ])


def replicate_eve(N):
    """Constructs and randomly initializes an Eve network.

    Args:
        N: int
            Length of messages (and keys) used for training.

    Returns: keras.models.Sequential
    """
    return Sequential([
        Dense(2 * N, activation='sigmoid', input_shape=(N, )),
        Reshape((2 * N, 1)),
        Conv1D(2, 4, strides=1, padding='same', activation='sigmoid'),
        Conv1D(4, 2, strides=2, padding='same', activation='sigmoid'),
        Conv1D(4, 1, strides=1, padding='same', activation='sigmoid'),
        Conv1D(1, 1, strides=1, padding='same', activation='tanh'),
        Reshape((N, ))
    ])


def crack_loss(y_true, y_pred):
    """Eve's contribution to Alice and Bob's loss function.

    Defined on page 6 of Abadi and Andersen 2016 under "Loss Functions".

    Args:
        y_true: tensorflow.Tensor
            Target values

        y_pred: tensorflow.Tensor
            Predicted values

    Returns: tensorflow.Tensor
    """
    N = K.int_shape(y_pred)[1] / 2.
    return K.pow(N - sae(y_true, y_pred), 2) / K.pow(N, 2)


def sae(y_true, y_pred):
    """Sum of the absolute errors or L1 error.

    Serves as the basis for the loss function used by Bob and Eve.

    Args:
        y_true: tensorflow.Tensor
            Target values

        y_pred: tensorflow.Tensor
            Predicted values

    Returns: tensorflow.Tensor
    """
    return K.sum(K.abs(y_true - y_pred), axis=-1) / 2


def generate_batch(N=16, batch_size=4096):
    """Creates a single training batch of keys and messages for experiment 1.

    Creates batch_size message-key pairs which have a combined lenght of 2 * N.
    Messages and keys are binary, but encoded using {-1, 1} rather than {0, 1}.

    Args:
        N: int (default: 16)
            Length of messages (and keys).

        batch_size: int (default: 4096)

    Returns: tuple
        (messages, keys)
        A tuple of numpy.ndarrays containing the messages and keys.
    """
    messages = randint(2, size=(batch_size, N)) * 2 - 1
    keys = randint(2, size=(batch_size, N)) * 2 - 1

    return messages, keys


if __name__ == '__main__':
    main()
