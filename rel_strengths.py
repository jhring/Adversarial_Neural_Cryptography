"""
Tests Alice, Bob, and Eve with random strengths.
"""

from os.path import isfile
import csv
from time import time
from keras import backend as K
from keras.layers import Input, concatenate, Conv1D, Dense, Reshape
from keras.models import Model
from keras.optimizers import Adam
from keras.utils import multi_gpu_model
from numpy.random import randint
from numpy import zeros, mean
import tensorflow as tf

def main():
    run()

def run(epochs=100000):

    while True:
        train(epochs=epochs)
        K.clear_session()

def train(epochs=100000,
          N=16,
          batch_size=4096,
          comm_iter=1,
          crack_iter=2,
          retrys=3,
          outfile='results.csv'):

    alice_dense = randint(1, 11)
    alice_fils = randint(1, 11, 3)
    eve_dense = randint(1, 11)
    eve_fils = randint(1, 11, 3)

    with tf.device('/cpu:0'):

        alice = make_alice(N, alice_dense, alice_fils)
        bob = make_alice(N, alice_dense, alice_fils)
        eve = make_eve(N, eve_dense, eve_fils)

        # Symbolic inputs for plain text and private key
        message = Input(shape=(N,))
        key = Input(shape=(N,))

        # Alice network I/O
        alice_in = concatenate([message, key])
        alice_pred = alice(alice_in)

        # Bob network I/O
        bob_in = concatenate([alice_pred, key])
        bob_pred = bob(bob_in)

        # Eve network I/O
        eve_pred = eve(alice_pred)

        comm_model = Model(inputs=[message, key], outputs=[bob_pred, eve_pred])

        crack_model = Model(inputs=[message, key], outputs=eve_pred)

    comm_model = multi_gpu_model(comm_model, gpus=2)
    crack_model = multi_gpu_model(crack_model, gpus=2)

    # Compile communication network so that Eve is frozen
    eve.trainable = False

    comm_model.compile(optimizer=Adam(lr=0.0008),
                       loss=[sae, crack_loss],
                       loss_weights=[1., 1.])

    # Compile crack network so that Alice is frozen
    eve.trainable = True
    alice.trainable = False

    crack_model.compile(optimizer=Adam(lr=0.0008), loss=sae)

    bob_errors = zeros(retrys)
    eve_errors = zeros(retrys)
    start = time()
    for i in range(retrys):
        for _ in range(epochs):
            for _ in range(comm_iter):
                messages, keys = generate_batch(N, batch_size=batch_size)
                comm_model.train_on_batch([messages, keys], [messages, messages])

            for _ in range(crack_iter):
                messages, keys = generate_batch(N, batch_size=batch_size)
                crack_model.train_on_batch([messages, keys], messages)

        messages, keys = generate_batch(N)
        _, bob_errors[i], _ = comm_model.test_on_batch([messages, keys],
                                                       [messages, messages])
        eve_errors[i] = crack_model.test_on_batch([messages, keys], messages)

    end = time() - start
    # write errors etc
    rows = []
    if not isfile(outfile):
        rows.append(['time', 'b_dense', 'b_f1', 'b_f2', 'b_f3', 'b_error',
                     'e_dense', 'e_f1', 'e_f2', 'e_f3', 'e_error'])

    out = [str(end), alice_dense]
    out.extend(alice_fils)
    out.extend([mean(bob_errors), eve_dense])
    out.extend(eve_fils)
    out.append(mean(eve_errors))

    rows.append(out)

    with open(outfile, 'a') as myfile:
        wrtr = csv.writer(myfile)
        for row in rows:
            wrtr.writerow(row)
        myfile.flush()

def make_alice(N, num_dense, fils):
    model_input = Input(shape=(2 * N, ))
    x = Dense(2 * N, activation='sigmoid')(model_input)

    for i in range(num_dense-1):
        x = Dense(2 * N, activation='sigmoid')(x)

    x = Reshape((2 * N, 1))(x)
    x = Conv1D(fils[0], 4, strides=1, padding='same', activation='sigmoid')(x)
    x = Conv1D(fils[1], 2, strides=2, padding='same', activation='sigmoid')(x)
    x = Conv1D(fils[2], 1, strides=1, padding='same', activation='sigmoid')(x)
    x = Conv1D(1, 1, strides=1, padding='same', activation='tanh')(x)
    x = Reshape((N, ))(x)

    model = Model(inputs=[model_input], outputs=[x])

    return model

def make_eve(N, num_dense, fils):
    model_input = Input(shape=(N, ))
    x = Dense(2 * N, activation='sigmoid')(model_input)

    for _ in range(num_dense-1):
        x = Dense(2 * N, activation='sigmoid')(x)

    x = Reshape((2 * N, 1))(x)
    x = Conv1D(fils[0], 4, strides=1, padding='same', activation='sigmoid')(x)
    x = Conv1D(fils[1], 2, strides=2, padding='same', activation='sigmoid')(x)
    x = Conv1D(fils[2], 1, strides=1, padding='same', activation='sigmoid')(x)
    x = Conv1D(1, 1, strides=1, padding='same', activation='tanh')(x)
    x = Reshape((N, ))(x)

    model = Model(inputs=[model_input], outputs=[x])
    return model

def sae(y_true, y_pred):
    return K.sum(K.abs(y_true - y_pred), axis=-1) / 2

def crack_loss(y_true, y_pred):
    N = K.int_shape(y_pred)[1] / 2.
    return K.pow(N - sae(y_true, y_pred), 2) / K.pow(N, 2)

def generate_batch(N=16, batch_size=4096):
    messages = randint(2, size=(batch_size, N)) * 2 - 1
    keys = randint(2, size=(batch_size, N)) * 2 - 1

    return messages, keys

if __name__ == '__main__':
    main()
