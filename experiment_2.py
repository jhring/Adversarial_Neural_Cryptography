"""
Replicates experiment 2 of 'Learning to Protect Communications with Adversarial
Neural Cryptography' by Abadi and Andersen.
"""


from keras import backend as K
from keras.layers import Input, concatenate, Conv1D, Dense, Reshape, Lambda
from keras.models import Model, Sequential
from keras.optimizers import Adam
from numpy.random import randint
import matplotlib.pyplot as plt
import numpy as np
import json


def main():
    # experiment_2()

    # experiment_2_report()

    # _, a_errors, b_errors, e_errors, blind_errors = experiment_2_train()

    # a_errors = list(a_errors)
    # b_errors = list(b_errors)
    # e_errors = list(e_errors)
    # blind_errors = list(blind_errors)

    # with open('experiment_2_errors.json', 'w') as f:
    #     json.dump([a_errors, b_errors, e_errors, blind_errors], f)

    gen_figure_4()
    gen_figure_4(file_type='png')


def experiment_2(epochs=500000):
    pass


def experiment_2_report():
    pass


def gen_figure_4(fig_name='figure_4', file_type='pdf'):
    with open('experiment_2_errors.json', 'r') as f:
            a_errors, b_errors, e_errors, blind_errors = json.load(f)

    fig, ax = plt.subplots()
    ax.plot(np.arange(len(a_errors)),
            a_errors,
            'r-',
            linewidth=0.5,
            label='Public Error')
    ax.plot(np.arange(len(b_errors)),
            b_errors,
            'g-',
            linewidth=0.5,
            label='Bob Error')
    ax.plot(np.arange(len(e_errors)),
            np.asarray(e_errors) - np.asarray(blind_errors),
            'b-',
            linewidth=0.5,
            label='Eve - Blind Eve')

    ax.set_xlabel('Steps')
    ax.set_ylabel('Error')

    ax.legend()

    plt.grid(True)

    plt.savefig('{}.{}'.format(fig_name, file_type), dpi=96)


def experiment_2_train(epochs=500000,
                       comm_iter=1,
                       crack_iter=2,
                       batch_size=4096):
    # Symbolic input variables for the adverasrial networks
    message = Input(shape=(4,))
    key = Input(shape=(8,))

    # Contruct the component networks
    alice = build_network(input_dim=12, first_layer=12)
    bob = build_network(input_dim=14, first_layer=14)
    eve = build_network(input_dim=6, first_layer=6)
    eve_blind = build_network(input_dim=5, first_layer=6)

    # Create Alice's inputs and outputs
    alice_in = concatenate([message, key])
    alice_out = alice(alice_in)
    cipher_text = Lambda(lambda x: x[:, 1:])(alice_out)
    d_pub = Lambda(lambda x: x[:, 0])(alice_out)
    d_pub = Reshape((1, ))(d_pub)

    # Create Bob's inputs and outputs
    bob_in = concatenate([alice_out, key])
    bob_out = bob(bob_in)
    d_est = Lambda(lambda x: x[:, 0])(bob_out)
    d_est = Reshape((1, ))(d_est)

    # Create Eve's inputs and outputs
    eve_out = eve(alice_out)
    eve_c = Lambda(lambda x: x[:, 0])(eve_out)
    eve_c = Reshape((1, ))(eve_c)

    # Create blind Eve's inputs and outputs
    eve_blind_out = eve_blind(cipher_text)
    eve_blind_c = Lambda(lambda x: x[:, 0])(eve_blind_out)
    eve_blind_c = Reshape((1, ))(eve_blind_c)

    # Construct adversarial models using the above components
    alice_bob_net = Model(inputs=[message, key], outputs=[d_pub, d_est, eve_c])
    eve_net = Model(inputs=[message, key], outputs=[eve_c])
    eve_blind_net = Model(inputs=[message, key], outputs=[eve_blind_c])

    # Compile the adversarial models so that only desired networks are trained
    eve.Trainable = False
    eve_blind.Trainable = False

    alice_bob_net.compile(optimizer=Adam(lr=0.0008),
                          loss=['mse', 'mse', batch_covariance],
                          loss_weights=[1., 1., 1.])

    eve.Trainable = True
    alice.Trainable = False
    bob.Trainable = False

    eve_net.compile(optimizer=Adam(lr=0.0008), loss='mse')

    eve_blind.Trainable = True

    eve_blind_net.compile(optimizer=Adam(lr=0.0008), loss='mse')

    # Training
    alice_errors = np.zeros(epochs)
    bob_errors = np.zeros(epochs)
    eve_errors = np.zeros(epochs)
    blind_errors = np.zeros(epochs)

    for i in range(epochs):
        for j in range(comm_iter):
            messages, keys, C, D = generate_batch()
            alice_bob_net.train_on_batch([messages, keys],
                                         [D, D, C])

        for k in range(crack_iter):
            messages, keys, C, D = generate_batch()
            eve_net.train_on_batch([messages, keys], C)
            eve_blind_net.train_on_batch([messages, keys], C)

        messages, keys, C, D = generate_batch()
        _, alice_errors[i], bob_errors[i], _ = alice_bob_net.test_on_batch(
            [messages, keys], [D, D, C])

        eve_errors[i] = eve_net.test_on_batch([messages, keys], C)
        blind_errors[i] = eve_blind_net.test_on_batch([messages, keys], C)

    return alice, alice_errors, bob_errors, eve_errors, blind_errors


def build_network(input_dim=12, first_layer=10):
    """Constructs and randomly initializes an Alice, Bob, or Eve network.

    Args:
        input_dim: int
            Length of the network inputs.

        first_layer: int
            Number of neurons in the first fully connected layer.

    Returns: keras.models.Sequential
    """
    return Sequential([
        Dense(first_layer, activation='sigmoid', input_shape=(input_dim, )),
        Reshape((first_layer, 1)),
        Conv1D(2, 4, strides=1, padding='same', activation='sigmoid'),
        Conv1D(4, 2, strides=2, padding='same', activation='sigmoid'),
        Conv1D(4, 1, strides=1, padding='same', activation='sigmoid'),
        Conv1D(1, 1, strides=1, padding='same', activation='tanh'),
        Reshape((first_layer // 2, ))
    ])


def batch_covariance(y_true, y_pred):
    return K.sum((y_true - K.mean(y_true)) * (y_pred - K.mean(y_pred)))


def generate_batch(batch_size=4096):
    L = np.linalg.cholesky(create_SPD_array(4))

    data = np.dot(np.random.normal(size=(batch_size, 4)), L)
    C = data[:, 2]
    D = data[:, 3]
    keys = randint(2, size=(batch_size, 8)) * 2 - 1

    return data, keys, C, D


def create_SPD_array(size):
    A = np.random.random((size, size))
    A = 0.5 * (A + A.T) + size * np.eye(size)

    max_diag = np.max(np.diag(A))
    for i in range(size):
        A[i, i] = max_diag

    return A / max_diag


if __name__ == '__main__':
    main()
